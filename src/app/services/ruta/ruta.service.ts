import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RutaService {

  constructor(private httpClient: HttpClient) { }
  /**
    *header para peticiones post
    * @memberof EventoService
    */
  header = new HttpHeaders(
    {
      "Accept": 'application/json',
      'Content-Type': 'application/json',
      'response-Type': 'json'
    }

  );
  async obtenerRutas(rut, inicio, termino) {
    let consulta = null;
    let fechaI = this.ordenarFecha(inicio);
    let fechaT = this.ordenarFecha(termino);
   
    let url = `${environment.endPoint}${environment.methods.rutas}${rut.replace('.', '').replace('.', '')}/${fechaI}/${fechaT}`;
   
    consulta = await this.httpClient.get(url,{responseType: 'json'}).toPromise();

    return consulta.data;



  }



  /**
   *Permite ordenar la fecha para almacenar
   * @param {Date} fechaCompleta
   * @returns
   * @memberof EventoService
   */
  ordenarFecha(fechaCompleta: Date) {

    fechaCompleta = new Date(fechaCompleta);


    let dia = fechaCompleta.getDate();
    let mes = fechaCompleta.getMonth() + 1;
    let anio = fechaCompleta.getFullYear();
    let hora = fechaCompleta.getHours();
    let min = fechaCompleta.getMinutes();
    mes = mes > 12 ? 1 : mes;
    let diaSrt = dia < 10 ? '0' + dia : dia;
    let mesStr = mes < 10 ? '0' + mes : mes;
    let horaStr = hora < 10 ? '0' + hora : hora;
    let minStr = min < 10 ? '0' + min : min;

    let formatoFecha = `${anio}${mesStr}${diaSrt}`

    return formatoFecha

  }
}
