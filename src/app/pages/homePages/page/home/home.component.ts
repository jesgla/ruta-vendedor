import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { NgbModal, NgbCalendar, NgbDateParserFormatter, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { RutaService } from 'src/app/services/ruta/ruta.service';
import { environment } from 'src/environments/environment';
import { InfoWindow } from '@agm/core/services/google-maps-types';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  diaHoy: number;
  fromDate: any;
  toDate: any;
  formFechas: FormGroup;
  hoveredDate: NgbDate;
  vendedores: any[];
  markers: any[];
  center: google.maps.LatLngLiteral
  lng: number;
  lat: number;
  origin: { lat: number; lng: number; };
  destination: { lat: number; lng: number; };
  rutas: any;
  public renderOptions = {
    suppressMarkers: true,
  }
  public markerOptions = {
    origin: {
      label: '',
      draggable: true,
    },
    destination: {
      label: '',

    }
  };
  public infoWindow: InfoWindow = undefined;

  public obtainInfowindow(window: InfoWindow) {
    this.infoWindow = window
  }
  constructor(
    private formGroup: FormBuilder,
    private calendar: NgbCalendar,
    public formatter: NgbDateParserFormatter,
    private rutaService: RutaService
  ) {
    this.diaHoy = new Date().getDate();

    this.fromDate = calendar.getToday();
    this.vendedores = environment.vendedores.sort((a, b) => a.nombreCompleto < b.nombreCompleto ? -1 : null);

    this.toDate = calendar.getNext(calendar.getToday(), 'd', 0);
    this.formFechas = this.formGroup.group({
      vendedor: new FormControl(null, Validators.required),
      fechaInicio: new FormControl('', Validators.required),
      fechaTermino: new FormControl('', Validators.required)
    });
    this.markers = [];
    this.rutas = [];
  }

  ngOnInit() {
    navigator.geolocation.getCurrentPosition(position => {

      this.lat = position.coords.latitude
      this.lng = position.coords.longitude

    })
  };
  getDirection() {
    this.origin = { lat: this.lat, lng: this.lng }

    this.destination = { lat: -33.402856, lng: -70.5980748 }
    this.rutas.push({ origin: this.origin, destination: this.destination });
    let otro = { lat: -33.5837229, lng: -70.6724438 };
    this.rutas.push({ origin: this.origin, destination: otro });
    // this.origin = 'Taipei Main Station' -33.5837229, -70.6724438
    // this.destination = 'Taiwan Presidential Office' 
  }
  onDateSelection(date: NgbDate) {

    if (!this.fromDate && !this.toDate) {

      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.formFechas.controls['fechaTermino'].setValue(this.formatter.format(date))
      this.toDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;

      this.formFechas.controls['fechaInicio'].setValue(this.formatter.format(date))
    }

  }

  async obtenerRuta() {
    console.log('form', this.formFechas.value);
    const { vendedor, fechaInicio, fechaTermino } = this.formFechas.value;
    let rutas = await this.rutaService.obtenerRutas(vendedor, fechaInicio, fechaTermino);

    this.formatearMacas(rutas);


  }
  formatearMacas(_rutas) {
    this.rutas = [];
    _rutas.sort((a, b) => a.fechaReal > b.fechaReal ? -1 : null);

    console.log(_rutas);
    _rutas.map(async (ruta, index) => {
      let suma = index + 1;
      if (index < _rutas.length - 1) {

        let origin = { lat: Number(ruta.latitud), lng: Number(ruta.longitud) }

        let destination = { lat: Number(_rutas[suma].latitud), lng: Number(_rutas[suma].longitud) }


        if (index < suma && index !== suma) {
          let markerOptions = {
            origin: {
              label: index.toString(),
              draggable: true,
            },
            destination: {
              label: (suma).toString(),

            }
          }
      
          this.rutas.push({ origin: origin, destination: destination, markerOptions: markerOptions })
        }
        ;
      }


      /*  this.markers.push(
         {
           position: {
             lat: Number(ruta.latitud),
             lng: Number(ruta.longitud),
           },
           label: {
             color: 'red',
             text: 'Marker label ' + (this.markers.length + 1),
           },
           title: 'Marker title ' + (this.markers.length + 1),
           options: { animation: google.maps.Animation.BOUNCE },
         }); */
    });
  }
  onClick() {
    console.log('aqi');
  }
  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  validateInput(currentValue: NgbDate, input: string): NgbDate {

    const parsed = this.formatter.parse(input);
    return parsed && this.calendar.isValid(NgbDate.from(parsed)) ? NgbDate.from(parsed) : currentValue;
  }

}
