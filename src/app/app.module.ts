import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/homePages/page/home/home.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { GoogleMapsModule } from '@angular/google-maps';
import {NgbModule, NgbDatepickerI18n} from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction'
import { Traduccion, I18n } from './config/traduccion';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD_HuwF5F8X8fOSR_1Ai_hFT115caUq4vI'
    }),
    AgmDirectionModule,
  ],
  providers: [
    I18n,
    {provide: NgbDatepickerI18n, useClass: Traduccion},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
