// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  endPoint:"https://b2b-api.implementos.cl/",
  methods:{
    rutas:"api/localizacion/getlocalizacion/"
  },
  vendedores:[
        {
            "CodEmpleado": "516",
            "Clasificacion": "VENDEDOR ESPECIALIZADO",
            "Rut": "10835560-3",
            "nombreCorto": "EDUARDO SANTIBAÑEZ",
            "hdmWorker": "5637145344",
            "nombreCompleto": "EDUARDO BRAULIO SANTIBAÑEZ CONTRERAS"
        },
        {
            "CodEmpleado": "66",
            "Clasificacion": "VENDEDOR ESPECIALIZADO",
            "Rut": "11232786-K",
            "nombreCorto": "LUIS GAMBOA",
            "hdmWorker": "5637145351",
            "nombreCompleto": "LUIS ALBERTO GAMBOA GAMBOA"
        },
        {
            "CodEmpleado": "785",
            "Clasificacion": "VENDEDOR ESPECIALIZADO",
            "Rut": "9966251-4",
            "nombreCorto": "ROSA OYARCE",
            "hdmWorker": "5637145607",
            "nombreCompleto": "ROSA ANA OYARCE ROJAS"
        },
        {
            "CodEmpleado": "708",
            "Clasificacion": "VENDEDOR SUB DISTRIBUCION",
            "Rut": "17487559-6",
            "nombreCorto": "JORGE SANTOS",
            "hdmWorker": "5637145548",
            "nombreCompleto": "JORGE GONZALO SANTOS COLIL"
        },
        {
            "CodEmpleado": "678",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "12693800-4",
            "nombreCorto": "CAMILO HENRIQUEZ",
            "hdmWorker": "5637145394",
            "nombreCompleto": "CAMILO BENJAMIN HENRIQUEZ ROA"
        },
        {
            "CodEmpleado": "650",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "13647760-9",
            "nombreCorto": "CARLOS ARAYA",
            "hdmWorker": "5637145427",
            "nombreCompleto": "CARLOS ALBERTO ARAYA VILLAFAÑA"
        },
        {
            "CodEmpleado": "13",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "7645965-7",
            "nombreCorto": "CARLOS CASTRO",
            "hdmWorker": "5637145579",
            "nombreCompleto": "CARLOS ALBERTO CASTRO ACUÑA"
        },
        {
            "CodEmpleado": "20",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "10199750-2",
            "nombreCorto": "CARLOS RODRIGUEZ",
            "hdmWorker": "5637145329",
            "nombreCompleto": "CARLOS MIGUEL RODRIGUEZ TOBAR"
        },
        {
            "CodEmpleado": "437",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "12300981-9",
            "nombreCorto": "CESAR ARRIAGADA",
            "hdmWorker": "5637145378",
            "nombreCompleto": "CESAR ALEJANDRO ARRIAGADA BRITO"
        },
        {
            "CodEmpleado": "83",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "9519782-5",
            "nombreCorto": "CLAUDIO VALDES",
            "hdmWorker": "5637145603",
            "nombreCompleto": "CLAUDIO BENJAMIN VALDES CHANDIA"
        },
        {
            "CodEmpleado": "177",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "16781058-6",
            "nombreCorto": "CRISTIAN NOVOA",
            "hdmWorker": "5637145526",
            "nombreCompleto": "CRISTIAN JAVIER NOVOA CACERES"
        },
        {
            "CodEmpleado": "503",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "15442923-9",
            "nombreCorto": "CRISTIAN RAMIREZ",
            "hdmWorker": "5637145472",
            "nombreCompleto": "CRISTIAN ANDRES RAMIREZ ARENAS"
        },
        {
            "CodEmpleado": "628",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "15726500-8",
            "nombreCorto": "FELIPE ADRIAZOLA",
            "hdmWorker": "5637145485",
            "nombreCompleto": "FELIPE ADRIAZOLA GRANIFO"
        },
        {
            "CodEmpleado": "665",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "13297041-6",
            "nombreCorto": "FERNANDO RUBIO",
            "hdmWorker": "5637145416",
            "nombreCompleto": "FERNANDO CRISTIAN RUBIO ZAPATA"
        },
        {
            "CodEmpleado": "852",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "18489212-K",
            "nombreCorto": "GONZALO CACERES",
            "hdmWorker": "5637176854",
            "nombreCompleto": "GONZALO NICOLAS CACERES RODRIGUEZ"
        },
        {
            "CodEmpleado": "60",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "11250384-6",
            "nombreCorto": "HECTOR LEZANA",
            "hdmWorker": "5637145352",
            "nombreCompleto": "HECTOR PATRICIO LEZANA MEZA"
        },
        {
            "CodEmpleado": "657",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "15749518-6",
            "nombreCorto": "JAIME YAÑEZ",
            "hdmWorker": "5637145488",
            "nombreCompleto": "JAIME FELIPE YAÑEZ MAÑAN"
        },
        {
            "CodEmpleado": "29",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "8735710-4",
            "nombreCorto": "JOSE FLORES",
            "hdmWorker": "5637145592",
            "nombreCompleto": "JOSE ROBINSON FLORES OPAZO"
        },
        {
            "CodEmpleado": "788",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "9922273-5",
            "nombreCorto": "JUAN GONZALEZ",
            "hdmWorker": "5637145606",
            "nombreCompleto": "JUAN LUIS GONZALEZ RIVEROS"
        },
        {
            "CodEmpleado": "86",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "12991382-7",
            "nombreCorto": "JUAN GUZMAN",
            "hdmWorker": "5637145402",
            "nombreCompleto": "JUAN CARLOS GUZMAN CORDERO"
        },
        {
            "CodEmpleado": "320",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "12869719-5",
            "nombreCorto": "JUAN JARA",
            "hdmWorker": "5637145400",
            "nombreCompleto": "JUAN ANTONIO JARA HINOJOSA"
        },
        {
            "CodEmpleado": "45",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "12216390-3",
            "nombreCorto": "JUAN VERGARA",
            "hdmWorker": "5637145376",
            "nombreCompleto": "JUAN ANDRES VERGARA VALLEJOS"
        },
        {
            "CodEmpleado": "453",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "15309595-7",
            "nombreCorto": "LUIS OYARZO",
            "hdmWorker": "5637145469",
            "nombreCompleto": "LUIS CRISTIAN OYARZO OYARZO"
        },
        {
            "CodEmpleado": "38",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "8491443-6",
            "nombreCorto": "LUIS RAMIREZ",
            "hdmWorker": "5637145586",
            "nombreCompleto": "LUIS ENRIQUE RAMIREZ LASTARRIA"
        },
        {
            "CodEmpleado": "742",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "10492209-0",
            "nombreCorto": "MARIA CASTAÑEDA",
            "hdmWorker": "5637145336",
            "nombreCompleto": "MARIA DE LOS ANGELES CASTAÑEDA TRONCOSO"
        },
        {
            "CodEmpleado": "486",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "15221912-1",
            "nombreCorto": "PABLO ROMERO",
            "hdmWorker": "5637145465",
            "nombreCompleto": "PABLO ALBERTO ROMERO CABRERA"
        },
        {
            "CodEmpleado": "5",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "13445050-9",
            "nombreCorto": "PAULO BARRERA",
            "hdmWorker": "5637145420",
            "nombreCompleto": "PAULO ANDRES BARRERA BRAVO"
        },
        {
            "CodEmpleado": "557",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "15738975-0",
            "nombreCorto": "ROLANDO PEREZ",
            "hdmWorker": "5637145487",
            "nombreCompleto": "ROLANDO ISAIAS PEREZ PAVEZ"
        },
        {
            "CodEmpleado": "879",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "15106331-4",
            "nombreCorto": "SEBASTIAN OLIVARES",
            "hdmWorker": "5637185826",
            "nombreCompleto": "SEBASTIAN ANDRES OLIVARES OLIVARES"
        },
        {
            "CodEmpleado": "388",
            "Clasificacion": "VENDEDOR TERRENO",
            "Rut": "16831151-6",
            "nombreCorto": "VICTOR AVILA",
            "hdmWorker": "5637145527",
            "nombreCompleto": "VICTOR ALEJANDRO AVILA PEREZ"
        }
    ]

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
